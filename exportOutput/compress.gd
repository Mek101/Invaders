extends SceneTree


func __is_linux() -> bool:
	var os_name :String = OS.get_name()
	return os_name == "X11" || os_name == "Server"

func __add_arg(args :Array, new_arg :String) -> Array:
	var new_args :Array
	# Adds the option
	for a in args:
		new_args.append(new_arg + " " + a)
	
	return new_args

# Obtains the files.
func __get_libs(path :String, extension :String):
	var dir = Directory.new()
	var execs :Array

	print("Compress::Opening files in '%s'" % path);
	if dir.open(path) == OK:
		dir.list_dir_begin()
		# Starts from the first file
		var file_name = dir.get_next()
		
		while !file_name.empty():
			if (extension in file_name):
				var file_path = path + "/" + file_name
				execs.append(file_path)
			file_name = dir.get_next()
		
		return execs
	else:
		return null

# A wrapper func for calling 'OS.execute' from a different thread.
func __run_exec_wrapper(args :Array) -> void:
	__run_exec(args[0], args[1])

func __run_exec(exec :String, args :Array) -> void:
	var output :Array
	var ret_code :int = OS.execute(exec, args, true, output)
	if (ret_code != 0):
		printerr("Compress::Error while running '%s' with %s:\n\tret code '%s'\n\toutput %s" % [ exec, args, ret_code, output ])

# Retuns the path to the searched exec. Empty if not found.
func __find_exec(var name :String) -> String:
	var cmd :String
	var output :Array

	if (__is_linux()):
		cmd = "which"
	else:
		cmd = "where.exe"
	
	var ret_code :int = OS.execute(cmd, [ name ], true, output)

	if(ret_code == 0):
		return output[0].strip_edges() # Removing ending newline.
	else:
		return ""

func __apply_exec(cmd :String, var args :Array, parallel :bool) -> void:
	var path :String = __find_exec(cmd)
	if (path.empty()):
		printerr("Compress::ERROR '%s' not found. Skipping..." % cmd)
	else:
		pass #print("Compress::Running '%s' on %s" % [ cmd, args ])
		
		# Starts a new thread for each argument, then waits for each thread to finish.
		if (parallel):
			var threads :Array
			for a in args:
				var t:Thread = Thread.new()
				t.start(self, "__run_exec_wrapper", [ path, [ a ] ])
				threads.append(t)
			
			for t in threads:
				t.wait_to_finish()
		else:
			__run_exec(path, args)


# Works as a main function.
func _init():
	compress()
	quit()


func compress() -> void:
	# Gets the export-specific path to the single lib files.
	var lib_source_path :String = "Invaders/exportOutput/%s/data_Invaders/Mono/lib"
	var lib_paths :Array
	if (__is_linux()):
		lib_paths = __get_libs(lib_source_path % "linux", ".so")
	else:
		lib_paths = __get_libs(lib_source_path % "windows", ".dll")
	
	# On linux, the libs are also stripped before being compressed.
	if (__is_linux()):
		__apply_exec("strip", lib_paths, true)
	
	# Compressing the libs to the maximum.
	if (__is_linux()):
		# Temporarely enabling execution of the libs for upx.
		__apply_exec("chmod", __add_arg(lib_paths, "+x"), true)
		__apply_exec("upx", __add_arg(lib_paths, "--best -qq"), true)
		__apply_exec("chmod", __add_arg(lib_paths, "-x"), true)
	else:
		__apply_exec("upx", [ "--best -qq" ] + lib_paths, true)
	
	# Applies maximum compression to the game exec.
	var game_path :String = "Invaders/exportOutput/%s/Invaders.%s"
	if (__is_linux()):
		game_path = game_path % [ "linux", "x86_64" ]
		__apply_exec("strip", [ "-v", game_path ], false)
		__apply_exec("upx", [ "--best -qq", game_path ], false)
	else:
		__apply_exec("upx", [ "--best -qq", game_path % [ "windows", "exe"] ], false)
