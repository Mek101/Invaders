#!/bin/bash
files="Invaders.x86_64 linux/data_Invaders/Mono/lib/*.so"

echo "Compressing $files"

for lib in linux/data_Invaders/Mono/lib/*.so; do
	chmod +x $lib
done

strip -v $files
upx -9 $files

for lib in linux/data_Invaders/Mono/lib/*.so; do
	chmod -x $lib
done

