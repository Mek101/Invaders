import os

struct Tuple {
	success bool
	path string
}

fn log(log string, ok bool) {
	if ok {
		println("OK::$log")
	}
	else {
		println("FALIURE::$log")
	}
}

///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// Tools section.

const(
	strip = "strip"
	upx = "upx"
	compress = "compress"
	local_folder = "exportTools"
)



// get_from_system attamps to obtain the system-level tool
fn get_from_system(tool string) Tuple {

	// Setting the correct command.
	mut cmd := ""
	$if linux {
		cmd = "which $tool"
	}
	$else {
		cmd = "where $tool"
	}

	// Attempting to get the system tool path.
	res := os.exec(cmd) or {
		// This is bad.
		log("Error encountered with $cmd", false)
		return { success: false, path: "" }
	}

	
	if res.exit_code != 0 {
		log("Couldn't find the system-level tool with $cmd, exit code $res.exit_code", false)
		return { success: false, path: "" }
	}
	else {
		log("Found system level tool at $cmd", true)
		return { success: true, path: res.output }
	}
}


fn get_from_local(tool string) Tuple {
	mut path := ""

	$if linux {
		path = "$local_folder/$tool"
	}
	$else {
		path = "$local_folder\\$tool"
		path += ".exe"
	}

	if os.exists(path) {
		log("Found the local tool at $path", true)
		return { success: true, path: path }
	}
	else {
		log("Couldn't find the local tool at $path", false)
		return { success: false, path: "" }
	}
}


// get_tool attemps to obtain the system-level tool or the local tool
fn get_tool(tool string) ?string {
	// Trying to obtain the system-level tool
	sys_res := get_from_system(tool)

	
	if sys_res.success {
		return sys_res.path
	}
	// If failure, obtain the local one
	else {
		loc_res := get_from_local(tool)

		if loc_res.success {
			return loc_res.path
		}
		else {
			log("Couldn't find tool $tool at any level!", false)
			error("Couldn't find tool $tool at any level!")
		}
	}

	// Dummy, since the compiler isn't so clever yet.
	return "This shouldn't happen"
}



// get_tools_path obtains the path of the tools, or returns an error.
fn get_tools_path() ?map[string]string {
	mut paths := map[string]string

	paths[strip] = get_tool(strip) or {
		return err
	}
	paths[upx] = get_tool(upx) or {
		return err
	}
	
	$if linux {
		paths[compress] = get_tool("tar") or {
			return err
		}
	}
	$else {
		// Windows has no native command.
		res := get_from_local("zip.bat")
		if res.success {
			paths[compress] = res.path
		}
		else {
			error("Couldn't find the local tool at $res.path")
		}
	}

	return paths
}

///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// Files section.

// get_files_path obtains all the files to work on.
fn get_files_path() ?[]string {


	$if windows {

	}
	$else {

	}
}


///+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



fn main() {
	println("Starting compression...")


	mut errors := []string

	paths := get_tools_path() or {
		errors << err
	}

	files := get_files_path() or {
		errors << err
	}


	// Reports all the errors encountered.
	if errors.len != 0 {
		panic(errors)
	}
}