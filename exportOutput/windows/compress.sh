#!/bin/bash
files="Invaders.x86_64 data_Invaders/Mono/lib/*.so"

for lib in data_Invaders/Mono/lib/*.so; do
	chmod +x $lib
done

strip -v $files
upx -9 $files

for lib in data_Invaders/Mono/lib/*.so; do
	chmod -x $lib
done
