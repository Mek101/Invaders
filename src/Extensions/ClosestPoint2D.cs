
namespace Godot.Extensions
{
	public static class ClosestPoint2D
	{
		/// <summary>
		/// Determiantes the closest point from the line to the given source.
		/// Learned from: http://geomalgorithms.com/a02-_lines.html, "Distance of a Point to a Ray
		/// or Segment" paragraph.
		/// </summary>
		/// <param name="lineStartPos">The line start point.</param>
		/// <param name="lineEndPos">The line end point.</param>
		/// <param name="sourcePoint">The source point to find the nearest point of.</param>
		/// <returns>The point belonging to the line nearest to the source point.</returns>
		public static Vector2 ClosestPointToLine(Vector2 lineStartPoint, Vector2 lineEndPoint, Vector2 sourcePoint)
		{
			// Vector representing the line.
			Vector2 lineVector = lineEndPoint - lineStartPoint;

			// Vector from the line start to the source point.
			Vector2 lineStartToPoint = sourcePoint - lineStartPoint;

			// The dot product of the line vector with the source point.
			float dotLineStartToSource = lineStartToPoint.Dot(lineVector);

			// If it's before the start point, the nearest point is the start point.
			if (dotLineStartToSource <= 0)
				return lineStartPoint;

			// Estrapolating the dot product of the line vector end with the source point.
			float dotLineEndToSource = (lineVector * lineVector).Length();

			// If it's after the end point, the nearest point is the end point.
			if (dotLineEndToSource <= dotLineStartToSource)
				return lineEndPoint;

			// Calculating the destination point from the line vector
			return lineStartPoint + (dotLineStartToSource / dotLineEndToSource) * lineVector;
		}
	}
}