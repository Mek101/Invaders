using System;

namespace Godot.Extensions
{
	/// <remarks>
	/// Cannot use 'dynamic', since the first call is very heavy, causing ~0.500 sec delay, making
	/// the game stutter.
	/// </remarks>
	internal abstract class Invocable : Godot.Object
	{
		protected object _sender;


		public abstract void Invoke();


		public Invocable(object sender)
		{
			_sender = sender;
		}
	}


	internal class InvocableSimple : Invocable
	{
		private EventHandler _eventHandler;

		private EventArgs _args;


		private void InvokeImpl()
		{
			_eventHandler?.Invoke(_sender, _args);
			CallDeferred("free");
		}


		public InvocableSimple(EventHandler eventHandler, object sender, EventArgs args) : base(sender)
		{
			_eventHandler = eventHandler;
			_args = args;
		}

		public override void Invoke()
		{
			CallDeferred(nameof(InvokeImpl));
		}
	}


	internal class Invocable<T> : Invocable
	{
		private EventHandler<T> _eventHandler;

		private T _args;


		private void InvokeImpl()
		{
			_eventHandler?.Invoke(_sender, _args);
			CallDeferred("free");
		}


		public Invocable(EventHandler<T> eventHandler, object sender, T args) : base(sender)
		{
			_eventHandler = eventHandler;
			_args = args;
		}

		public override void Invoke()
		{
			CallDeferred(nameof(InvokeImpl));
		}
	}


	public static class ObjectEvents
	{
		/// <summary>
		/// Invokes an event in a dereferred way from the sender node.
		/// </summary>
		/// <param name="eventHandler">This event to invoke.</param>
		/// <param name="sender">The sender Node object.</param>
		/// <param name="e">The event arguments.</param>
		public static void DereferredInvoke(this EventHandler eventHandler, Godot.Object sender, EventArgs e)
		{
			Invocable invocable = new InvocableSimple(eventHandler, sender, e);
			invocable.Invoke();
		}

		/// <summary>
		/// Invokes an event in a dereferred way from the sender node.
		/// </summary>
		/// <param name="eventHandler">This event to invoke.</param>
		/// <param name="sender">The sender Node object.</param>
		/// <param name="e">The event arguments.</param>
		/// <typeparam name="T">The event arguments type.</typeparam>
		public static void DereferredInvoke<T>(this EventHandler<T> eventHandler, Godot.Object sender, T e)
		{
			Invocable invocable = new Invocable<T>(eventHandler, sender, e);
			invocable.Invoke();
		}
	}
}
