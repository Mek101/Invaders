using Godot.Extensions.ResourceHandlers;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Godot.Extensions
{
	/// <summary>
	/// The current loading status of the resource.
	/// </summary>
	public enum LoadStatus : int
	{
		/// <summary>
		/// The resource is not in memory and needs to be loaded before being accessed.
		/// </summary>
		NotLoaded,
		/// <summary>
		/// The resource is currently loading and needs to finishe loading before being accessed.
		/// </summary>
		Loading,
		/// <summary>
		/// The resource is loaded in memory and ready to be accessed.
		/// </summary>
		Loaded
	}


	/// <summary>
	/// Specifies how the resource should be loaded.
	/// </summary>
	public enum LoadMode : byte
	{
		/// <summary>
		/// The resource will be loaded immediately and cached into the global cache.
		/// </summary>
		Now,
		/// <summary>
		/// The resource will be loaded at every call.
		/// </summary>
		OnCall,
		/// <summary>
		/// The resource will be loaded and cached into the global cache the first time it is accessed.
		/// </summary>
		Lazy,
		/// <summary>
		/// The resource will be asyncronously loaded and cached into the global cache.
		/// </summary>
		Preload
	}

	public interface IPreloadedResource
	{
		LoadMode Mode { get; }

		LoadStatus Status { get; }

		int StageCount { get; }

		int Stage { get; }

		NodePath Path { get; }

		Resource Resource { get; }


		event EventHandler<int> OnStageLoaded;

		event EventHandler<int> OnResourceLoaded;
	}


	public interface IPreloadedResource<T> : IPreloadedResource where T : Resource
	{
		new T Resource { get; }
	}


	/// <summary>
	/// Provvides syncronous and asyncronous resource loading and preloading.
	/// </summary>
	public static class Preloader
	{
		/// <summary>
		/// Preloads the specified resource in the given mode, returning a handle to access it.
		/// Specifies when the resource should be loaded, and also provides a global, local or no
		/// cache for the loaded resources, depending on the mode.
		/// </summary> 
		/// <param name="path">The path to the resource to load.</param>
		/// <param name="mode">Specifies how and when the resource should be loaded and cached.</param>
		/// <typeparam name="T">The resource type.</typeparam>
		/// <returns>A handle to access the resource.</returns>
		public static IPreloadedResource<T> Preload<T>(NodePath path, LoadMode mode) where T: Resource
		{
			/**
			 * If a resource is already cached, don't waste time using lazy initialization or async
			 * stuff, and return the simplest object masked as the requested load mode.
			 */
			if(ResourceLoader.HasCached(path))
				return new WrapperResource<T>(GD.Load<T>(path), mode);

			switch(mode)
			{
				case LoadMode.Now:
					return new WrapperResource<T>(GD.Load<T>(path), LoadMode.Now);
				case LoadMode.Lazy:
					return new LazyResource<T>(path);
				case LoadMode.Preload:
					return new AsyncResource<T>(path);
				default:
					throw new ArgumentOutOfRangeException(nameof(mode));
			}
		}

		/// <summary>
		/// Preloads the specified resource in the given mode, returning a handle to access it.
		/// Specifies when the resource should be loaded, and also provides a global, local or no
		/// cache for the loaded resources, depending on the mode.
		/// </summary> 
		/// <param name="path">The path to the resource to load.</param>
		/// <param name="mode">Specifies how and when the resource should be loaded and cached.</param>
		/// <returns>A handle to access the resource.</returns>
		public static IPreloadedResource Preload(NodePath path, LoadMode mode)
		{
			return Preload<Resource>(path, mode);
		}

		/// <summary>
		/// Preloads the specified resource in the given mode, returning a handle to access it.
		/// Specifies when the resource should be loaded, and also provides a global, local or no
		/// cache for the loaded resources, depending on the mode.
		/// </summary> 
		/// <param name="path">The path to the resource to load.</param>
		/// <param name="mode">Specifies how and when the resource should be loaded and cached.</param>
		/// <typeparam name="T">The resource type.</typeparam>
		/// <returns>A handle to access the resource.</returns>
		public static IDictionary<NodePath, IPreloadedResource<T>> Preload<T>(IEnumerable<NodePath> paths, LoadMode mode)
			where T: Resource
		{
			return paths.Select((p) => Preload<T>(p, mode)).ToDictionary((r) => r.Path);
		}

		/// <summary>
		/// Preloads the specified resource in the given mode, returning a handle to access it.
		/// Specifies when the resource should be loaded, and also provides a global, local or no
		/// cache for the loaded resources, depending on the mode.
		/// </summary> 
		/// <param name="path">The path to the resource to load.</param>
		/// <param name="mode">Specifies how and when the resource should be loaded and cached.</param>
		/// <returns>A handle to access the resource.</returns>
		public static IDictionary<NodePath, IPreloadedResource> Preload(IEnumerable<NodePath> paths, LoadMode mode)
		{
			return (IDictionary<NodePath, IPreloadedResource>)Preload<Resource>(paths, mode);
		}
	}
}
