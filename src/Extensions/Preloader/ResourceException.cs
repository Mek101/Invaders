using System.IO;

namespace Godot.Extensions
{
	public class ResourceException : IOException
	{
		public Error Error { get; private set; }
		
		
		public ResourceException(Error error) : base("Resource loading error: " + error.ToString() + "")
		{
			Error = error;
		}
	}
}