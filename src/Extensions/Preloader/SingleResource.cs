using Godot.Extensions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Godot.Extensions.ResourceHandlers
{
	internal class LazyResource<T> : IPreloadedResource<T> where T : Resource
	{
		private T _lazyResource;

		/// <summary>
		/// Status backend implementation usable with Interlocked.
		/// </summary>
		private int _status;

		/// <summary>
		/// Initialization status to prevent multiple inizializations of _lazyResource.
		/// </summary>
		private bool _initialized;

		/// <summary>
		/// Syncronization object to prevent multiple inizializations of _lazyResource.
		/// </summary>
		private object _syncObject;

        public T Resource => GetResource();

		public LoadMode Mode => LoadMode.Lazy;

		public LoadStatus Status
		{
			get => (LoadStatus)Interlocked.Exchange(ref _status, _status);
			private set => Interlocked.Exchange(ref _status, (int)value);
		}

		public NodePath Path { get; private set; }

		Resource IPreloadedResource.Resource => GetResource();

		public int StageCount => 1;

		public int Stage => _initialized ? 1 : 0;


		public event EventHandler<int> OnStageLoaded;
        public event EventHandler<int> OnResourceLoaded;


		private T GetResource()
		{
			return LazyInitializer.EnsureInitialized(ref _lazyResource, ref _initialized, ref _syncObject, delegate()
			{
				Status = LoadStatus.Loading;
				T res = GD.Load<T>(Path);
				Status = LoadStatus.Loaded;

				OnStageLoaded?.Invoke(this, 1);
				OnResourceLoaded?.Invoke(this, 1);

				return res;
			});
		}


		public LazyResource(NodePath path)
		{
			Status = LoadStatus.NotLoaded;
			_initialized = false;
			_syncObject = new object();
		}
	}


	internal class AsyncResource<T> : IPreloadedResource<T> where T : Resource
	{
		private readonly Task<T> _loader;

		private readonly ResourceInteractiveLoader _interactiveLoader;


        public T Resource => _loader.Result;

		public LoadMode Mode => LoadMode.Preload;

		public LoadStatus Status => GetStatus();

		public NodePath Path { get; private set; }

		Resource IPreloadedResource.Resource => _loader.Result;

		public int StageCount => _interactiveLoader.GetStageCount();

		public int Stage => _interactiveLoader.GetStage();


		public event EventHandler<int> OnStageLoaded;
		public event EventHandler<int> OnResourceLoaded;


		private void RaiseStageLoaded()
		{
			OnStageLoaded?.Invoke(this, _interactiveLoader.GetStage());
		}

		/// <summary>
		/// The loading status is deduced by the _loader task status.
		/// </summary>
		/// <returns>The corresponding load status to the loader task status.</returns>
		private LoadStatus GetStatus()
		{
			switch(_loader.Status)
			{
				case TaskStatus.Running:
					return LoadStatus.Loading;
				case TaskStatus.RanToCompletion:
					return LoadStatus.Loaded;
				case TaskStatus.Created:
				default:
					return LoadStatus.NotLoaded;
			}
		}

		private T Load()
		{
			while(true)
			{
				Error status = _interactiveLoader.Poll();

				switch(status)
				{
					case Error.FileEof:
						RaiseStageLoaded();
						OnResourceLoaded?.Invoke(this, _interactiveLoader.GetStageCount());
						return (T)_interactiveLoader.GetResource();
					case Error.Ok:
						RaiseStageLoaded();
						continue;
					default:
						throw new ResourceException(status);
				}
			}
		}


		public AsyncResource(NodePath path)
		{
			Path = path;
			_interactiveLoader = ResourceLoader.LoadInteractive(Path);
			_loader = Task.Run(Load);
		}
	}


	/// <summary>
	/// The simplest implementation, which just wraps the resource. Used for the Now load mode and
	/// to mask cached resources already loaded.
	/// </summary>
	/// <typeparam name="T">The resource type.</typeparam>
	internal class WrapperResource<T> : IPreloadedResource<T> where T : Resource
	{
		private readonly T _resource;


        public T Resource => _resource;

		public LoadMode Mode { get; private set; }

		public LoadStatus Status => LoadStatus.Loaded;

		public NodePath Path => _resource.ResourcePath;

		Resource IPreloadedResource.Resource => _resource;


		// Dummy events: the wrapper is loaded from the start.
		public event EventHandler<int> OnStageLoaded;
		public event EventHandler<int> OnResourceLoaded;


		public int StageCount => 1;
		public int Stage => 1;


		/// <summary>
		/// Instantiates a WrapperResource object.
		/// </summary>
		/// <param name="resource">The wrapped resource.</param>
		/// <param name="mode">Mode parameter.</param>
		public WrapperResource(T resource, LoadMode mode)
		{
			_resource = resource;
			Mode = mode;
		}
	}
}