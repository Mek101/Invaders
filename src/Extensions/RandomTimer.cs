using Godot.Extensions;
using System;
using System.Threading;

namespace Godot.Extensions
{
	public class RandomTimer : Node
	{
		/// <summary>
		/// Timeout signal from the _timer.
		/// </summary>
		private const string TIMEOUT = "timeout";

		private Godot.Timer _timer;

		private int _oneShotSwitch;

		private int _isStoppedSwitch;


		/// <summary>
		/// If <c>true</c> the timer will stop after the timeout, otherwise if <c>false</c> it will
		/// restart.
		/// </summary>
		/// <value><c>true</c> if enabled, <c>false</c> otherwise.</value>
		public bool OneShot
		{
			get => Interlocked.Exchange(ref _oneShotSwitch, _oneShotSwitch) == 1;
			set => Interlocked.Exchange(ref _oneShotSwitch, value ? 1 : 0); // C# doesn't support casting from bool to int.
		}

		public Godot.Timer.TimerProcessMode ProcessMode
		{
			get => _timer.ProcessMode;
			set => _timer.ProcessMode = value;
		}

		public bool IsStopped
		{
			get => Interlocked.Exchange(ref _isStoppedSwitch, _isStoppedSwitch) == 1;
			set => Interlocked.Exchange(ref _isStoppedSwitch, value ? 1 : 0); // C# doesn't support casting from bool to int.
		}

		/// <summary>
		/// Effective wait time in seconds.
		/// </summary>
		public float WaitTime => _timer.WaitTime;

		/// <summary>
		/// The minimum wait time before the timeout in seconds.
		/// </summary>
		[Export] public float MinWaitTime;

		/// <summary>
		/// The maximum wait time before the timeout in seconds.
		/// </summary>
		[Export] public float MaxWaitTime;


		/// <summary>
		/// Called whenthe timer expires.
		/// </summary>
		/// <param name="sender">The RandomTiemr sender instance.</value>
		/// <param name="e">The elapsed wait time.</value>
		public event EventHandler<float> OnTimeout;


		/// <summary>
		/// Internal callback for the internal timer.
		/// </summary>
		private void OnTimeoutCallback()
		{
			// Invokes the event in a safe dereferred way.
			OnTimeout?.DereferredInvoke(this, _timer.WaitTime);
			if(!OneShot && !IsStopped)
				Start();
		}


		public RandomTimer()
		{
			_timer = new Godot.Timer();
			_timer.OneShot = true;

			OneShot = false;
		}

		/// <summary>
		/// Starts the timer. This also resets the remaining time. Note: this method will not resume
		/// a paused timer. See Godot.Timer.Paused.
		/// </summary>
		public void Start()
		{
			if(IsInstanceValid(_timer))
			{
				IsStopped = false;
				if(!_timer.IsConnected(TIMEOUT, this, nameof(OnTimeoutCallback)))
					_timer.Connect(TIMEOUT, this, nameof(OnTimeoutCallback));
				_timer.Start((float)GD.RandRange(MinWaitTime, MaxWaitTime));
			}
		}
		 
		/// <summary>
		/// Stops the timer.
		/// </summary>
		/// <param name="timeout">Indicates if the timer should timeout when stopping.</param>
		public void Stop(bool timeout)
		{
			if(IsInstanceValid(_timer))
			{
				IsStopped = true;
				// Removing the signal beforehand connection since stopping the timer triggers the timeout.
				if(!timeout && _timer.IsConnected(TIMEOUT, this, nameof(OnTimeoutCallback)))
					_timer.Disconnect(TIMEOUT, this, nameof(OnTimeoutCallback));
				_timer.Stop();
			}
		}

		public override void _Ready()
		{
			if (MinWaitTime > MaxWaitTime)
				throw new ArgumentException("The " + nameof(MinWaitTime) + " (" + MinWaitTime +
					") cannot be higher than the " + nameof(MaxWaitTime) + " (" + MaxWaitTime + ").");

			// Setting up the timer...
			AddChild(_timer);
		}
	}
}