using System.Threading.Tasks;

namespace Godot.Extensions
{
	public enum InterpolationMember : byte
	{
		Method,
		Property
	}


	internal interface IInterpolable
	{
		/// <summary>
		/// Starts an interpolation with the given tween using the internal data.
		/// </summary>
		/// <param name="tween">The tween to interpolate with.</param>
		Task InterpolateWithAsync(Tween tween);

		/// <summary>
		/// Freeing the underliying object.
		/// </summary>
		void Free();
	}

	internal sealed class AnyCallbackInterpolation : Godot.Object, IInterpolable
	{
		private const string TWEEN_COMPLEATED = "tween_completed";
		private readonly bool _derefer;
		private readonly Godot.Object _object;
		private readonly float _duration;
		private readonly string _callback;
		private readonly object[] _args;


		public AnyCallbackInterpolation(bool derefer, Godot.Object @object, float duration,
			string callback, object[] args)
		{
			_derefer = derefer;
			_object = @object;
			_duration = duration;
			_callback = callback;

			if(args.Length < 5)
			{
				_args = new object[5];
				args.CopyTo(_args, 0);
			}
			else
				_args = args;
		}

		public AnyCallbackInterpolation(bool derefer, Object @object, float duration, string callback,
			object arg1 = null, object arg2 = null, object arg3 = null, object arg4 = null,
			object arg5 = null)
		{
			_derefer = derefer;
			_object = @object;
			_duration = duration;
			_callback = callback;
			_args = new object[] { arg1, arg2, arg3, arg4, arg5 };
		}

		public async Task InterpolateWithAsync(Tween tween)
		{
			if(_derefer)
			{
				tween.InterpolateDeferredCallback(_object, _duration, _callback, _args[0], _args[1],
					_args[2], _args[3], _args[4]);
			}
			else
			{
				tween.InterpolateCallback(_object, _duration, _callback, _args[0], _args[1], _args[2],
					_args[3], _args[4]);
			}

			tween.Start();

			// Removes the compleated interpolation after the interpolation ends.
			await ToSignal(tween, TWEEN_COMPLEATED);

			tween.Remove(_object, _callback);
		}
	}

	/// <summary>
	/// Provides interpolation for Properties and Methods.
	/// </summary>
	internal sealed class MemberInterpolation : Godot.Object, IInterpolable
	{
		private const string TWEEN_COMPLEATED = "tween_completed";
		private readonly InterpolationMember _mode;
		private readonly Godot.Object _object;
		private readonly NodePath _property;
		private readonly System.Object _initialVal;
		private readonly System.Object _finalVal;
		private readonly float _duration;
		private readonly Tween.TransitionType _transitionType;
		private readonly Tween.EaseType _easeType;
		private readonly float _delay;


		public MemberInterpolation(InterpolationMember mode, Godot.Object @object, NodePath property,
			System.Object initialVal, System.Object finalVal, float duration,
			Tween.TransitionType transType, Tween.EaseType easeType, float delay)
		{
			_mode = mode;
			_object = @object;
			_property = property;
			_initialVal = initialVal;
			_finalVal = finalVal;
			_duration = duration;
			_transitionType = transType;
			_easeType = easeType;
			_delay = delay;
		}

		public async Task InterpolateWithAsync(Tween tween)
		{
			if(_mode == InterpolationMember.Method)
			{
				tween.InterpolateMethod(_object, _property, _initialVal, _finalVal, _duration,
				 _transitionType, _easeType, _delay);
			}
			else
			{
				tween.InterpolateProperty(_object, _property, _initialVal, _finalVal, _duration,
				 _transitionType, _easeType, _delay);
			}

			tween.Start();

			// Removes the compleated interpolation after the interpolation ends.
			await ToSignal(tween, TWEEN_COMPLEATED);

			tween.Remove(_object, _property);
		}
	} 
}
