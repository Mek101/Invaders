using Godot;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Godot.Extensions
{
	/// <summary>
	/// A chain of tweens to execute sequentially.
	/// </summary>
	public class TweenChain : Node
	{
		/// <summary>
		/// Tween complete signal.
		/// </summary>
		private const string TWEEN_COMPLETE = "tween_completed";

		/// <summary>
		/// The tween in which operated.
		/// </summary>
		private readonly Tween _tween;

		/// <summary>
		/// The various Interpolations to play.
		/// </summary>
		private readonly IReadOnlyList<IInterpolable> _interpolations;


		/// <summary>
		/// The number of tweens in the chain.
		/// </summary>
		public int Count => _interpolations.Count;
		

		/// <summary>
		/// Raised when the chain has been compleated.
		/// </summary>
		public event EventHandler OnChainCompleated;

		/// <summary>
		/// Raised every time a tween completes.
		/// </summary>
		public event EventHandler OnTick;


		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach(IInterpolable interpolable in _interpolations)
					interpolable.Free();
			}
		}


		/// <summary>
		/// Instantiates a new tween chain.
		/// </summary>
		/// <param name="data">List of all the tweens to process.</param>
		/// <param name="tween">Tween already in the scene tree to operate on.</param>
		internal TweenChain(IReadOnlyList<IInterpolable> interpolations, Tween tween)
		{
			_tween = tween;
			_interpolations = interpolations;
		}


		/// <summary>
		/// Starts processing a chain. A chain already processing cannot initiate a new processing,
		/// and will return a canceled task.
		/// </summary>
		/// <returns>A the processing handle.</returns>
		public async Task StartAsync()
		{
			foreach(IInterpolable interpolation in _interpolations)
			{
				await interpolation.InterpolateWithAsync(_tween);
				OnTick?.DereferredInvoke(this, EventArgs.Empty);
			}

			OnChainCompleated?.DereferredInvoke(this, EventArgs.Empty);
		}
	}
}
