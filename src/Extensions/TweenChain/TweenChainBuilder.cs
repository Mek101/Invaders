using Godot;
using System;
using System.Collections.Generic;

namespace Godot.Extensions
{
	/// <summary>
	/// Builds a tween chain.
	/// </summary>
	public class TweenChainBuilder : IDisposable
	{
		private List<IInterpolable> _interpolations;


		/// <summary>
		/// The number of appended tweens.
		/// </summary>
		public int Count => _interpolations.Count;


		/// <summary>
		/// Instantiates a new builder.
		/// </summary>
		public TweenChainBuilder()
		{
			_interpolations = new List<IInterpolable>();
		}

		public void Dispose()
		{
			_interpolations.Clear();
			_interpolations = null;
		}

		/// <summary>
		/// Removes all the currently appended Tweens, resetting the builder.
		/// </summary>
		public void Clear()
		{
			_interpolations.Clear();
		}

		#region Base Append Implementations
		/// <summary>
		/// Adds a new callback interpolation to the chain in construction.
		/// </summary>
		/// <param name="derefer">Specifies if the callback shold be dereferred.</param>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>>
		/// <param name="callback">The callback to interpolate.</param>
		/// <param name="args">The array with up to five optional arguments.</param>
		public void AppendCallbackInterpolation(bool derefer, Godot.Object @object, float duration,
			string callback, object[] args)
		{
			_interpolations.Add(new AnyCallbackInterpolation(derefer, @object, duration, callback,
				args));
		}

		/// <summary>
		/// Adds a new callback interpolation to the chain in construction.
		/// </summary>
		/// <param name="derefer">Specifies if the callback shold be dereferred.</param>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>>
		/// <param name="callback">The callback to interpolate.</param>
		/// <param name="arg1">The first optional callback argument.</param>
		/// <param name="arg2">The second optional optional callback argument.</param>
		/// <param name="arg3">The third optional callback argument.</param>
		/// <param name="arg4">The fourth optional callback argument.</param>
		/// <param name="arg5">The fifth optional callback argument.</param>
		public void AppendCallbackInterpolation(bool derefer, Godot.Object @object, float duration,
			string callback, object arg1 = null, object arg2 = null, object arg3 = null,
			object arg4 = null, object arg5 = null)
		{
			_interpolations.Add(new AnyCallbackInterpolation(derefer, @object, duration, callback,
				arg1, arg2, arg3, arg4, arg5));
		}

		/// <summary>
		/// Adds a new object's member interpolation to the chain in construction.
		/// </summary>
		/// <param name="mode">The interpolation mode.</param>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="property">Property of the object to operate on.</param>
		/// <param name="initialVal">Starting value of Property of the object to operate on.</param>
		/// <param name="finalVal">Ending value to reach of the property of the object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>
		/// <param name="transType">The type of tween translation.</param>
		/// <param name="easeType">Ease type.</param>
		/// <param name="delay">Delay for the tween to start.</param>
		public void AppendMemberInterpolation(InterpolationMember mode, Godot.Object @object,
			NodePath property, System.Object initialVal,System.Object finalVal, float duration,
			Tween.TransitionType transType = Tween.TransitionType.Linear,
			Tween.EaseType easeType = Tween.EaseType.InOut, float delay = 0)
		{
			_interpolations.Add(new MemberInterpolation(mode, @object, property, initialVal, finalVal,
				duration, transType, easeType, delay));
		}
		#endregion // Base Append Implementations

		#region Simplified Append Implementations
		/// <summary>
		/// Adds a new callback interpolation to the chain in construction.
		/// </summary>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>>
		/// <param name="callback">The callback to interpolate.</param>
		/// <param name="args">The array with up to five optional arguments.</param>
		public void AppendCallbackInterpolation(Godot.Object @object, float duration, string callback,
			object[] args)
		{
			_interpolations.Add(new AnyCallbackInterpolation(false, @object, duration, callback, args));
		}

		/// <summary>
		/// Adds a new callback interpolation to the chain in construction.
		/// </summary>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>>
		/// <param name="callback">The callback to interpolate.</param>
		/// <param name="arg1">The first optional callback argument.</param>
		/// <param name="arg2">The second optional optional callback argument.</param>
		/// <param name="arg3">The third optional callback argument.</param>
		/// <param name="arg4">The fourth optional callback argument.</param>
		/// <param name="arg5">The fifth optional callback argument.</param>
		public void AppendCallbackInterpolation(Godot.Object @object, float duration, string callback,
			object arg1 = null, object arg2 = null, object arg3 = null, object arg4 = null,
			object arg5 = null)
		{
			_interpolations.Add(new AnyCallbackInterpolation(false, @object, duration, callback,
				arg1, arg2, arg3, arg4, arg5));
		}

		/// <summary>
		/// Adds a new derefered callback interpolation to the chain in construction.
		/// </summary>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>>
		/// <param name="callback">The callback to interpolate.</param>
		/// <param name="args">The array with up to five optional arguments.</param>
		public void AppendDerefferedCallbackInterpolation(Godot.Object @object, float duration,
			string callback, object[] args)
		{
			_interpolations.Add(new AnyCallbackInterpolation(true, @object, duration, callback, args));
		}

		/// <summary>
		/// Adds a new derefered callback interpolation to the chain in construction.
		/// </summary>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>>
		/// <param name="callback">The callback to interpolate.</param>
		/// <param name="arg1">The first optional callback argument.</param>
		/// <param name="arg2">The second optional optional callback argument.</param>
		/// <param name="arg3">The third optional callback argument.</param>
		/// <param name="arg4">The fourth optional callback argument.</param>
		/// <param name="arg5">The fifth optional callback argument.</param>
		public void AppendDerefferedCallbackInterpolation(Godot.Object @object, float duration,
			string callback, object arg1 = null, object arg2 = null, object arg3 = null,
			object arg4 = null, object arg5 = null)
		{
			_interpolations.Add(new AnyCallbackInterpolation(true, @object, duration, callback,
				arg1, arg2, arg3, arg4, arg5));
		}

		/// <summary>
		/// Adds a new object's property interpolation to the chain in construction.
		/// </summary>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="property">Property of the object to operate on.</param>
		/// <param name="initialVal">Starting value of Property of the object to operate on.</param>
		/// <param name="finalVal">Ending value to reach of the property of the object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>
		/// <param name="transType">The type of tween translation.</param>
		/// <param name="easeType">Ease type.</param>
		/// <param name="delay">Delay for the tween to start.</param>
		public void AppendPropertyInterpolation(Godot.Object @object, NodePath property,
			System.Object initialVal, System.Object finalVal, float duration,
			Tween.TransitionType transType = Tween.TransitionType.Linear,
			Tween.EaseType easeType = Tween.EaseType.InOut, float delay = 0)
		{
			_interpolations.Add(new MemberInterpolation(InterpolationMember.Property, @object, property,
				initialVal, finalVal, duration, transType, easeType, delay));
		}

		/// <summary>
		/// Adds a new object's member to the chain in construction.
		/// </summary>
		/// <param name="@object">Object to operate on.</param>
		/// <param name="property">Property of the object to operate on.</param>
		/// <param name="initialVal">Starting value of Property of the object to operate on.</param>
		/// <param name="finalVal">Ending value to reach of the property of the object to operate on.</param>
		/// <param name="duration">Time in seconds the tween must operate within.</param>
		/// <param name="transType">The type of tween translation.</param>
		/// <param name="easeType">Ease type.</param>
		/// <param name="delay">Delay for the tween to start.</param>
		public void AppendMemberInterpolation(Godot.Object @object, NodePath property,
			System.Object initialVal, System.Object finalVal, float duration,
			Tween.TransitionType transType = Tween.TransitionType.Linear,
			Tween.EaseType easeType = Tween.EaseType.InOut, float delay = 0)
		{
			_interpolations.Add(new MemberInterpolation(InterpolationMember.Method, @object, property,
				initialVal, finalVal, duration, transType, easeType, delay));
		}
		#endregion // Simplified Append Implementations

		/// <summary>
		/// Instantiates a new Tween chain, then resets the builder. Can be
		/// provided with a custom Tween already in the scene tree for the
		/// chain to operate on and consume.
		/// </summary>
		/// <param name="customTween">A custom tween already in the scene tree to operate on and consume.</param>
		/// <returns>A chain of tweens.</returns>
		public TweenChain BuildChain(Tween customTween = null)
		{
			// True if no custom tween was given.
			bool localTween = (customTween == null);
			if(localTween)
				customTween = new Tween();

			_interpolations.TrimExcess();
			TweenChain chain = new TweenChain(_interpolations, customTween);

			// The local tween is set as a chid of the chain
			if(localTween)
				chain.AddChild(customTween);

			_interpolations = new List<IInterpolable>();
			return chain;
		}
	}
}
