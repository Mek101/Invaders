using System;

namespace Godot.Extensions
{
	// Cannot be a generic class: https://github.com/godotengine/godot/issues/16706#issuecomment-394605337 because reasons.	
	public class VariantBox : Godot.Object
	{
		private object _item;
		

		public VariantBox() { }

		public VariantBox(object item)
		{
			_item = item;
		}

		public bool ItemIs(Type type)
		{
			return type.IsAssignableFrom(_item.GetType());
		}

		public bool ItemIs<T>()
		{
			return _item is T;
		}

		public void ItemSet(object value)
		{
			_item = value;
		}

		public object ItemGet()
		{
			return _item;
		}

		public void ItemSetAs<T>(T value)
		{
			_item = value;
		}

		public T ItemGetAs<T>()
		{
			return (T)_item;
		}
	}
}