using Godot;
using Invaders.Levels;

namespace Invaders
{
	public enum Difficulty : int 
	{
		Easy = -50,
		Medium = 0,
		Difficult = +50
	}

	/// <summary>
	/// Manages the combine and shared data of the game.
	/// </summary>
	public static class GameManager
	{
		public static Difficulty Difficulty { get; private set; }
		public static float DifficultyAddendum => ((float)(int)Difficulty) / 100;

		public static Sprite Background;
		public static Level CurrentLevel;


		static GameManager()
		{
			Difficulty = Difficulty.Medium;
		}
	}
}