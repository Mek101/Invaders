using Godot;
using Godot.Extensions;
using System;
using Invaders.Assets;
using Invaders.Bullets;

namespace Invaders.Aliens
{
	public enum CommonAliens { Fighter }

	/// <summary>
	/// Base class for any type of alien. Implements blinking, hit and destroyed audios, playing the
	/// default animation playing and taking damage.
	/// </summary>
	public abstract class BaseAlien : KinematicBody2D, IHittable
	{
		/// <summary>
		/// Cached blink provider.
		/// </summary>
		private BlinkProvider _blinker;

		/// <summary>
		/// The audio player with the sound of getting hit.
		/// </summary>
		private AudioStreamPlayer _hitAudio;

		/// <summary>
		/// The audio player with the sound of being destroyed.
		/// </summary>
		private AudioStreamPlayer _destroyedAudio;


		/// <summary>
		/// Current direction of the alien.
		/// </summary>
		protected Vector2 Direction;


		/// <summary>
		/// Tells whenever the alien should move or attack. Use for game over. True by default.
		/// </summary>
		/// <value><c>true</c> if the alien will move and attack, <c>false</c> if it will stay idle.</value>
		public bool IsActive;

		/// <summary>
		/// Alien health adjusted by difficulty, between -25% and +25% of the BaseHealth.
		/// </summary>
		public float AdjustedHealth;

		/// <summary>
		/// The current health of the alien.
		/// </summary>
		public float CurrentHealth;

		/// <summary>
		/// Base alien health.
		/// </summary>
		[Export] public float BaseHealth;

		/// <summary>
		/// How much accelleration is gained at each full frame, in perchentage. Must be between 0.0
		/// and 1.0.
		/// </summary>
		[Export(PropertyHint.Range, "0,1,0.01,or_greater")] public float PerchentageAccelleration;

		[Export] public float HorizontalSpeed;
		[Export] public float VerticalSpeed;

		/// <summary>
		/// The default animation to pick somewhere in the project.
		/// </summary>
		[Export] public Animation DefaultAnimation;


		/// <summary>
		/// Raised dereferred when the aliens is destroyed.
		/// </summary>
		public event EventHandler OnDestroyed;


		/// <summary>
		/// Get's a random direction axis.
		/// </summary>
		/// <returns>A random integer value -1 or 1.</returns>
		private float GetRandomDirection()
		{
			if(GD.Randi() % 2 == 1)
				return 1.0F;
			else 
				return -1.0F;
		}


		/// <summary>
		/// Moves the alien according to it's current state.
		/// </summary>
		/// <param name="delta">The time (in milliseconds) since the last physics processing cycle.</param>
		protected abstract void Move(float delta);


		/// <summary>
		/// Inflict damage to the alien.
		/// </summary>
		/// <param name="damage">The damage the alien is going to take.</param>
		/// <returns>The damage effectively taken by the alien.</returns>
		public float TakeDamage(float damage)
		{
			CurrentHealth -= damage + damage * GameManager.CurrentLevel.DifficultyFactor;

			if(CurrentHealth <= 0 && !_destroyedAudio.Playing)
			{
				_blinker.Blink();

				// Tansfers and plays asyncronously the destruction audio.
				var _ = AudioPlayerServer.PlaySoundTransferAsync(_destroyedAudio);

				// Derefers the event invoking since this method is usually ran during physics.
				OnDestroyed?.DereferredInvoke(this, EventArgs.Empty);

				// Destroys the alien.
				QueueFree();
			}
			else
			{
				_blinker.BlinkFor(25.0F);
				_hitAudio.Play();
			}

			return damage;
		}

		/// <summary>
		/// Called when the node enters the scene tree for the first time.
		/// </summary>
		public override void _Ready()
		{
			// Randomly goes left or right, and always descends.
			Direction = new Vector2(GetRandomDirection(), 1);

			// Setting a local material copy and the related blinker.
			Sprite sprite = GetNode<Sprite>("Sprite");
			ShaderMaterial localCopy = (ShaderMaterial)sprite.Material.Duplicate();

			sprite.Material = localCopy;
			_blinker = new ShaderMaterialWhiteBlink(localCopy);

			// Calculating and adjusting the real health of the alien on the difficulty.
			AdjustedHealth = BaseHealth + (BaseHealth / 2) * GameManager.CurrentLevel.DifficultyFactor;
			CurrentHealth = AdjustedHealth;

			// Setting the audio side of things.
			_hitAudio = GetNode<AudioStreamPlayer>("HitAudio");
			_destroyedAudio = GetNode<AudioStreamPlayer>("DestroyedAudio");

			// Starting the default animation if present.
			AnimationPlayer animationPlayer = GetNode<AnimationPlayer>("AnimationPlayer");
			animationPlayer.AddAnimation("default", DefaultAnimation);
			animationPlayer.Play("default");

			IsActive = true;
			// Setting the alien to unactive if the game ends
			GameManager.CurrentLevel.OnGameOver += (s, e) => IsActive = false;
		}

		/// <summary>
		/// Called every frame. Updates the alien position if IsActive is <c>true</c>.
		/// </summary>
		/// <param name="delta">Elapsed time since the previous frame.</param>
		public override void _PhysicsProcess(float delta)
		{
			if(IsActive)
				Move(delta);
		}
	}
}
