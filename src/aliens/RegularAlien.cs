using Godot;
using Godot.Extensions;
using System;
using Invaders.Bullets;

namespace Invaders.Aliens
{
	public enum AlienState : byte { Moving, Descending }

	/// <summary>
	/// Any alien that descends towards the gates in a snake pattern and shoots. Implements the
	/// default animation name, movement and shooting.
	/// </summary>
	public abstract class RegularAlien : BaseAlien
	{
		/// <summary>
		/// How many pixels the alien has still to descend before returning to move orizzontally.
		/// </summary>
		private float _descendLeft;

		/// <summary>
		/// Generates a timed event to make the alien shoot.
		/// </summary>
		private RandomTimer _shootingTimer;
		
		/// <summary>
		/// The audio player with the sound of shooting a bullet.
		/// </summary>
		private AudioStreamPlayer _shootAudio;


		/// <summary>
		/// State of the alien. Determinates the movement behaviour.
		/// </summary>
		public AlienState State { get; protected set; }


		[Export] public PackedScene BulletScene;

		/// <summary>
		/// The minimum delay (in seconds) between a bullet being shoot and the next.
		/// </summary>
		[Export] public float MinShootingDelay
		{
			get => _shootingTimer.MinWaitTime;
			set => _shootingTimer.MinWaitTime = value;
		}

		/// <summary>
		/// The maximum delay (in seconds) between a bullet being shoot and the next.
		/// </summary>
		[Export] public float MaxShootDelay
		{
			get => _shootingTimer.MaxWaitTime;
			set => _shootingTimer.MaxWaitTime = value;
		}

		/// <summary>
		/// How much shooting delay is removed at each full frame, in perchentage. Must be between
		/// 0.0 and 1.0.
		/// </summary>
		[Export(PropertyHint.Range, "0,1,0.01,or_greater")] public float ShootDelayAccelleration;

		/// <summary>
		/// The maximum descend (in pixels) the alien does while set in descending state.
		/// </summary>
		[Export] public float MinDescend;

		/// <summary>
		/// The minimum descend (in pixels) the alien does while set in descending state.
		/// </summary>
		[Export] public float MaxDescend;


		/// <summary>
		/// Spawns and initializes a bullet. Called on timeout.
		/// </summary>
		private void SpawnBullet()
		{
			// With Item1 -> starting point, Item2 -> ending point.
			ValueTuple<Vector2, Vector2> sidePoints = new ValueTuple<Vector2, Vector2>();
			
			// Getting the line points.
			sidePoints = GameManager.CurrentLevel.Fort.CurrentGate.GetTopSidePoints();

			// Determinates where to shoot
			Vector2 bulletDest = ClosestPoint2D.ClosestPointToLine(sidePoints.Item1, sidePoints.Item2,
				GlobalPosition);

			// Spawn and initialization.
			Bullet b = (Bullet)BulletScene.Instance();
			b.GlobalPosition = GlobalPosition;
			// Gets the angle between the bullet spawn point and the destination.
			b.GlobalRotation = GlobalPosition.AngleToPoint(bulletDest) - Mathf.Pi;
			// Points at the destination.
			b.Direction = GlobalPosition.DirectionTo(bulletDest);

			BulletAttachServer.AddBullet(b);

			// Play shooting sound.
			_shootAudio.Play();

			// Sligtly decreaes the shooting timer.
			float variance = (1 + GameManager.CurrentLevel.DifficultyFactor) * ShootDelayAccelleration;
			DecreaseMinShootingTimer(variance);
			DecreaseMaxShootingTimer(variance);
		}

		/// <summary>
		/// Sets the alien to descend a random ammount and not move horizzontally.
		/// </summary>
		private void SetDescendState()
		{
			State = AlienState.Descending;
			// Preserving at least 3 decimals...
			_descendLeft = (float)GD.RandRange(MinDescend, MaxDescend);
		}

		/// <summary>
		/// Sets the alien to move horizzontally and not vertically.
		/// </summary>
		private void SetMoveState()
		{
			State = AlienState.Moving;
			Direction.x *= -1; // Inverts orizontal movement direction.
			_descendLeft = 0;
		}

		private bool DecreaseMinShootingTimer(float value)
		{
			if(_shootingTimer.MaxWaitTime - value < 0.1F)
			{
				_shootingTimer.MaxWaitTime = 0.1F;
				return false;
			}
			else
			{
				_shootingTimer.MaxWaitTime -= value;
				return true;
			}
		}

		private bool DecreaseMaxShootingTimer(float value)
		{
			if(_shootingTimer.MaxWaitTime - value < 1.2F)
			{
				_shootingTimer.MaxWaitTime = 1.2F;
				return false;
			}
			else
			{
				_shootingTimer.MaxWaitTime -= value;
				return true;
			}
		}


		/// <summary>
		/// Moves the alien according to it's current state, changing it on collisions.
		/// </summary>
		protected override void Move(float delta)
		{
			// Calculates the speed addendum related to the game difficulty.
			float da = GameManager.CurrentLevel.DifficultyFactor * PerchentageAccelleration *
				(float)GD.RandRange(0.85, 1.25);

			Vector2 velocity;
			if(State == AlienState.Moving)
			{
				velocity = new Vector2(Direction.x * HorizontalSpeed * delta, 0);
				
				// Gaining speed each full frame.
				HorizontalSpeed += (HorizontalSpeed * PerchentageAccelleration + da) * delta;
			}
			else
			{
				float descend = Direction.y * VerticalSpeed * delta;
				// Gaining speed each full frame.
				VerticalSpeed += (VerticalSpeed * PerchentageAccelleration + da) * delta;

				velocity = new Vector2(0, descend);
				_descendLeft -= descend;
				if(_descendLeft <= 0)
					SetMoveState();
			}

			KinematicCollision2D collision = MoveAndCollide(velocity);

			/**
			 * If it has hit something while moving (another ship or a level side), changes the
			 * movement type.
			*/
			if(collision != null)
			{
				if(State == AlienState.Moving)
					SetDescendState();
				else if(State == AlienState.Descending && collision.Collider is BaseAlien)
					SetMoveState();
			}
		}


		public RegularAlien()
		{
			_shootingTimer = new RandomTimer();
			_shootingTimer.OneShot = false;
			_shootingTimer.OnTimeout += (s, e) => SpawnBullet();
		}

		public override void _Ready()
		{
			// Decreasing them minimum and maximum wait time for each shoot according to the difficulty.
			float df = GameManager.CurrentLevel.DifficultyFactor;

			DecreaseMinShootingTimer(_shootingTimer.MinWaitTime * df);
			DecreaseMaxShootingTimer(_shootingTimer.MaxWaitTime * df);

			GameManager.CurrentLevel.OnGameOver += (s, e) => _shootingTimer.Stop(false);
			AddChild(_shootingTimer);
			_shootingTimer.Start();

			// Setting up the shooting audio
			_shootAudio = GetNode<AudioStreamPlayer>("ShootAudio");

			base._Ready();
		}
	}
}
