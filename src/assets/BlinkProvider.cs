
namespace Invaders.Assets
{
	public abstract class BlinkProvider
	{
		private System.Timers.Timer _timer;


		/// <summary>
		/// Indicates if the shader is set to blink.
		/// </summary>
		/// <value><c>true</c> if the shader is set to whiten the material, <c>false</c> otherwise.</value>
		public abstract bool IsBlinking { get; }


		public BlinkProvider()
		{
			_timer = new System.Timers.Timer();
			_timer.AutoReset = false;
			_timer.Elapsed += (s, e) => Unblink();
		}

		/// <summary>
		/// Makes the target blink.
		/// </summary>
		public abstract void Blink();

		/// <summary>
		/// Stops the target blinking.
		/// </summary>
		public abstract void Unblink();

		/// <summary>
		/// Keep the target blinking for the set amount of time (milliseconds).
		/// </summary>
		/// <param name="milliseconds">The number of milliseconds the target should blink for.</param>
		public void BlinkFor(float milliseconds = 50.0F)
		{
			Blink();
			_timer.Interval = milliseconds;
			_timer.Start();
		}
	}
}