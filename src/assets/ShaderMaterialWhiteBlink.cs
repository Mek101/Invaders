using Godot;
using System;

namespace Invaders.Assets
{
	public class ShaderMaterialWhiteBlink : BlinkProvider
	{
		private const string WHITENING = "whitening";
		private const float ENABLE = 1.0F;
		private const float DISABLE = 0.0F;
		private ShaderMaterial _targetMaterial;

		public override bool IsBlinking => _targetMaterial.GetShaderParam(WHITENING).Equals(ENABLE);


		public ShaderMaterialWhiteBlink(ShaderMaterial targetMaterial)
		{
			if(targetMaterial.GetShaderParam(WHITENING) == null)
				throw new ArgumentException("Target shader material missing parameter \"" + WHITENING + "\".", nameof(targetMaterial));
			_targetMaterial = targetMaterial;
		}

		public override void Blink()
		{
			_targetMaterial.SetShaderParam(WHITENING, ENABLE);
		}

		public override void Unblink()
		{
			_targetMaterial.SetShaderParam(WHITENING, DISABLE);
		}
	}
}