using Godot;
using System.Threading.Tasks;

namespace Invaders
{
	public class AudioPlayerServer : Node
	{
		/// <summary>
		/// The audio finished signal, called when an audio playback ends.
		/// </summary>
		private const string FINISHED = "finished";

		private static AudioPlayerServer _instance;


		private static async Task ConsumePlayerAsync(AudioStreamPlayer player)
		{
			SignalAwaiter awaiter = _instance.ToSignal(player, FINISHED);
			_instance.AddChild(player);

			// Play and asyncronously wait for it to finish.
			player.Play();
			await awaiter;

			// Delete the audio player.
			_instance.RemoveChild(player);
			player.QueueFree();
		}


		/// <summary>
		/// Plays asyncronously a copy of the sound from a given sound player.
		/// </summary>
		/// <param name="soundPlayer">The sound player to copy.</param>
		/// <returns>Handler expiring at the audio finishing.</returns>
		public static Task PlaySoundCopyAsync(AudioStreamPlayer player)
		{
			AudioStreamPlayer copy = (AudioStreamPlayer)player.Duplicate();
			return ConsumePlayerAsync(copy);
		}

		/// <summary>
		/// Transfer the node and play asyncronously the sound from a given sound player.
		/// </summary>
		/// <param name="soundPlayer">The sound player to copy.</param>
		/// <returns>Handler expiring at the audio finishing.</returns>
		public static Task PlaySoundTransferAsync(AudioStreamPlayer player)
		{
			Node parent = player.GetParent();
			if(parent != null)
				parent.RemoveChild(player);

			return ConsumePlayerAsync(player);
		}

		/// <summary>
		/// Called when the node enters the scene tree for the first time.
		/// </summary>
		public override void _Ready()
		{
			if(_instance == null)
				_instance = this;
		}
	}
}