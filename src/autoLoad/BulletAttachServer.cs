using Godot;
using Invaders.Bullets;

namespace Invaders
{
	/// <summary>
	/// Provides an always present and loaded node to attach the existing bullets to
	/// prevent them from unloading when their shooters are freed.
	/// </summary>
	public sealed class BulletAttachServer : Node
	{
		private static BulletAttachServer _instance;


		public static void AddBullet(Bullet bullet)
		{
			_instance.AddChild(bullet);
		}

		public override void _Ready()
		{
			if(_instance == null)
				_instance = this;
		}
	}
}