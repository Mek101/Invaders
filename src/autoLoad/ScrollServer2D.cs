using Godot;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Invaders
{
	internal class MultiScroller : Node
	{
		private readonly Node2D[] _nodes;

		/// <summary>
		/// Starting positions of each Y-axis to use as base for the steps.
		/// </summary>
		private readonly float[] _startYs;


		public void InterpolateStep(float step)
		{
			for(int i = 0; i < _nodes.Length; i++)
			{
				// Bases each new node position adding the step to the original starting Ys.
				float newY = _startYs[i] - step;
				_nodes[i].Position = new Vector2(_nodes[i].Position.x, newY);
			}
		}

		public MultiScroller(Node2D[] nodes)
		{
			_nodes = nodes;
			_startYs = new float[_nodes.Length];
			for(int i = 0; i < _nodes.Length; i++)
				_startYs[i] = _nodes[i].Position.y;
		}
	}


	public class ScrollServer2D : Node
	{
		private const string NODE2D_POSITION = "position";
		private const string TWEEN_COMPLEATED = "tween_completed";

		private static ScrollServer2D _instance;
		private static List<Node2D> _scrollableSubscribers;


		public static object SyncRoot => _scrollableSubscribers;


		private static async Task ScrollCollectionWith(Node2D[] nodes, Tween tween, float amount, float duration)
		{
			MultiScroller scroller = new MultiScroller(nodes);
			_instance.AddChild(scroller);

			tween.InterpolateMethod(scroller, "InterpolateStep", 0, amount, duration);
			tween.Start();

			await _instance.ToSignal(tween, TWEEN_COMPLEATED);

			scroller.QueueFree();
		}


		static ScrollServer2D()
		{
			_scrollableSubscribers = new List<Node2D>();
		}

		/// <summary>
		/// Scrolls the given node with the given tween by the amount for the given duration.
		/// </summary>
		/// <param name="node">The node to scroll.</param>
		/// <param name="tween">The tween to scroll with.</param>
		/// <param name="amount">The amount of scrolling required.</param>
		/// <param name="duration">The duration of the animation.</param>
		/// <returns>A Task handle to wait completion.</returns>
		public static async Task ScrollWith(Node2D node, Tween tween, float amount, float duration)
		{
			Vector2 destPos = node.Position;
			destPos.y -= amount;

			tween.InterpolateProperty(node, NODE2D_POSITION, node.Position, destPos, duration);
			tween.Start();

			await _instance.ToSignal(tween, TWEEN_COMPLEATED);
		}

		/// <summary>
		/// Scrolls the given node with an internal tween by the amount for the given duration.
		/// </summary>
		/// <param name="node">The node to scroll.</param>
		/// <param name="amount">The amount of scrolling required.</param>
		/// <param name="duration">The duration of the animation.</param>
		/// <returns>A Task handle to wait completion.</returns>
		public static async Task Scroll(Node2D node, float amount, float duration)
		{
			Tween tween = new Tween();
			_instance.AddChild(tween);
			
			await ScrollWith(node, tween, amount, duration);

			tween.QueueFree();
		}

		/// <summary>
		/// Scrolls the given nodes with the given tween by the amount for the given duration.
		/// </summary>
		/// <param name="node">The node to scroll.</param>
		/// <param name="tween">The tween to scroll with.</param>
		/// <param name="amount">The amount of scrolling required.</param>
		/// <param name="duration">The duration of the animation.</param>
		/// <returns>A Task handle to wait completion.</returns>
		public static Task ScrollAllWith(Tween tween, float amount, float duration, params Node2D[] nodes)
		{
			return ScrollCollectionWith(nodes, tween, amount, duration);
		}

		/// <summary>
		/// Scrolls the given nodes with an internal tween by the amount for the given duration.
		/// </summary>
		/// <param name="node">The node to scroll.</param>
		/// <param name="tween">The tween to scroll with.</param>
		/// <param name="amount">The amount of scrolling required.</param>
		/// <param name="duration">The duration of the animation.</param>
		/// <returns>A Task handle to wait completion.</returns>
		public static async Task ScollAll(float amount, float duration, params Node2D[] nodes)
		{
			Tween tween = new Tween();
			_instance.AddChild(tween);

			await ScrollCollectionWith(nodes, tween, amount, duration);

			tween.QueueFree();
		}

		/// <summary>
		/// Scrolls the subscribed nodes with the given tween by the amount for the given duration.
		/// </summary>
		/// <param name="node">The node to scroll.</param>
		/// <param name="tween">The tween to scroll with.</param>
		/// <param name="amount">The amount of scrolling required.</param>
		/// <param name="duration">The duration of the animation.</param>
		/// <returns>A Task handle to wait completion.</returns>
		public static Task ScrollSubscribersWith(Tween tween, float amount, float duration)
		{
			// Copying in a local array to prevent data racing.
			Node2D[] nodesCopy;
			lock(_scrollableSubscribers)
				nodesCopy = _scrollableSubscribers.ToArray();

			return ScrollCollectionWith(nodesCopy, tween, amount, duration);
		}

		/// <summary>
		/// Scrolls the subscribed nodes with an internal tween by the amount for the given duration.
		/// </summary>
		/// <param name="node">The node to scroll.</param>
		/// <param name="amount">The amount of scrolling required.</param>
		/// <param name="duration">The duration of the animation.</param>
		/// <returns>A Task handle to wait completion.</returns>
		public static async Task ScrollSubscribers(float amount, float duration)
		{
			Tween tween = new Tween();
			_instance.AddChild(tween);

			// Copying in a local array to prevent data racing.
			Node2D[] nodesCopy;
			lock(_scrollableSubscribers)
				nodesCopy = _scrollableSubscribers.ToArray();

			await ScrollCollectionWith(nodesCopy, tween, amount, duration);

			tween.QueueFree();
		}

		/// <summary>
		/// Subscribes a node to be scrolled upon call.
		/// </summary>
		/// <param name="subsciber"></param>
		public static void Subscribe(Node2D subsciber)
		{
			lock(_scrollableSubscribers)
				_scrollableSubscribers.Add(subsciber);
		}

		public static bool UnSubscribe(Node2D subscribed)
		{
			lock(_scrollableSubscribers)
				return _scrollableSubscribers.Remove(subscribed);
		}

		public static bool IsSubscribed(Node2D subscribed)
		{
			lock(_scrollableSubscribers)
				return _scrollableSubscribers.Contains(subscribed);
		}

		public override void _Ready()
		{
			if(_instance == null)
				_instance = this;
		}
	}
}