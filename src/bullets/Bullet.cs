using Godot;

namespace Invaders.Bullets
{
	public abstract class Bullet : Area2D
	{
		private const string SCREEN_EXITED = "screen_exited";
		private const string BODY_ENTERED = "body_entered";


		public Vector2 Direction;
		[Export] public float Speed;
		[Export] public float Damage;


		public void OnBodyHit(Node body)
		{
			IHittable hittable = body as IHittable;
			if(hittable != null)
			{
				hittable.TakeDamage(Damage);
				QueueFree(); // The bullet self-destroys once it has hit something.
			}
		}

		public override void _Ready()
		{
			SetAsToplevel(true);
			// Using Godot's ugly event system, with the snake case method name for native methods.
			GetNode<VisibilityNotifier2D>("Visibility").Connect(SCREEN_EXITED, this, "queue_free");
			Connect(BODY_ENTERED, this, nameof(OnBodyHit));
		}

		public override void _PhysicsProcess(float delta)
		{
			Position += Direction * Speed * delta;
		}
	}
}
