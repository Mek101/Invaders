using Godot;
using System;

namespace Invaders.Bullets
{
	/// <summary>
	/// Represents a body that can be hit and take damage.
	/// </summary>
	public interface IHittable
	{
		/// <summary>
		/// Inflict damage to the body.
		/// </summary>
		/// <param name="damage">The damage the body is going to take.</param>
		/// <returns>The damage effectively taken by the body.</returns>
		float TakeDamage(float damage);
	}
}

