using Godot;
using System;
using Invaders.Pit;

namespace Invaders.Fortress
{
	public class Fort : Node2D
	{
		private PitController _pitController;


		[Export] public float ScrollDuration;

		public Gate CurrentGate => _pitController.GateManager.CurrentGate;

		public event EventHandler<int> OnGateDestroyed;

		public event EventHandler OnAllGatesDestroyed;


		/// <summary>
		/// Scrolls the fort and background along the X axis
		/// </summary>
		private void Scroll()
		{
			var _ = ScrollServer2D.ScrollSubscribers(_pitController.PitSectionHeight, ScrollDuration);
		}


		// Called when the node enters the scene tree for the first time.
		public override void _Ready()
		{
			ScrollServer2D.Subscribe(this);

			_pitController = GetNode<PitController>("PitController");

			_pitController.GateManager.OnGateDestroyed += (s, e) => Scroll();
			_pitController.GateManager.OnGateDestroyed += (s, e) => OnGateDestroyed?.Invoke(s, e);
			_pitController.GateManager.OnAllGatesDestroyed += (s, e) => OnAllGatesDestroyed?.Invoke(s, e);
		}
	}
}
