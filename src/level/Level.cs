using Godot;
using Godot.Extensions;
using System;
using Invaders.Fortress;
using Invaders.Turrets;

namespace Invaders.Levels
{
	public class Level : Node2D
	{
		public SpawnPointController SpawnPointController  { get; protected set; }

		public TurretDockController TurretDockController  { get; protected set; }

		public Fort Fort { get; protected set; }

		public int WaveNumber { get; protected set; }

		/// <summary>
		/// Goes from 0.0 inward. Is 0.(WaveNumber - 1) + the game's global difficulty addendum.
		/// </summary>
		/// <returns></returns>
		public float DifficultyFactor { get; protected set; }


		/// <summary>
		/// Raised on game over. The argument is <c>true</c> if the player has won, <c>false</c>
		/// otherwise.
		/// </summary>
		public event EventHandler<bool> OnGameOver;

		/// <summary>
		/// Raise on advancing the wave. The argument is the wave number.
		/// </summary>
		public event EventHandler<int> OnWaveAdvance;


		private void AdvanceWave()
		{
			WaveNumber++;

			if(WaveNumber == 1)
				DifficultyFactor = GameManager.DifficultyAddendum;
			else
				DifficultyFactor = Mathf.Log(WaveNumber - 1) + GameManager.DifficultyAddendum;

			OnWaveAdvance?.DereferredInvoke(this, WaveNumber);
		}


		public Level()
		{
			WaveNumber = 0;
			GameManager.CurrentLevel = this; // This is the only level...
		}

		public void Start()
		{
			SpawnPointController.SpawnStart();
		}

		public override void _Ready()
		{
			SpawnPointController = GetNode<SpawnPointController>("SpawnPointController");
			TurretDockController = GetNode<TurretDockController>("TurretDockController");
			Fort = GetNode<Fort>("Fort");

			Fort.OnAllGatesDestroyed += (s, e) => SpawnPointController.SpawnStop();
			Fort.OnAllGatesDestroyed += (s, e) => OnGameOver?.Invoke(this, false);

			AdvanceWave();
			Start();
		}
	}
}