using Godot;
using Invaders.Aliens;

namespace Invaders.Levels
{
	public class SpawnPoint : Node2D
	{
		public void Spawn(PackedScene alienScene, float spawnScale)
		{
			// Spawns the thing.
			BaseAlien a = (BaseAlien)alienScene.Instance();
			AddChild(a);
			// Divided by scale to adjust the position scaling with a scale different than 1.
			a.Position = Position / spawnScale;
			a.Scale = new Vector2(spawnScale, spawnScale);

			GD.Print(a.GetType().ToString() + " spawned at " + a.GlobalPosition + " at spawnpoint "
				+ Name + ": " + GlobalPosition);
		}
	}
}