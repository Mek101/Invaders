using Godot;
using Godot.Extensions;
using System;
using System.Linq;
using System.Threading;
using Invaders.Aliens;

namespace Invaders.Levels
{
	public class SpawnPointController : Node2D
	{
		private RandomTimer _spawnTimer;
		private SpawnPoint[] _spawnPoints;
		private IPreloadedResource<PackedScene>[] _commonAliens;
		private int _spawnedAliens;


		/// <summary>
		/// The number of aliens spawned in this wave.
		/// </summary>
		public int SpawnedAliens
		{
			get => Interlocked.Exchange(ref _spawnedAliens, _spawnedAliens);
			set => Interlocked.Exchange(ref _spawnedAliens, value);
		}

		/// <summary>
		/// The number of aliens per each wave.
		/// </summary>
		[Export(PropertyHint.Range, "1,2,1,or_greater")] public int AliensPerWave = 1;

		/// <summary>
		/// The scale of the spawned aliens.
		/// </summary>
		/// <value>The global scaling of the spawned aliens.</value>
		[Export] public float SpawnedScale = 1;

		[Export(PropertyHint.Range, "0,1,0.01,or_greater")] public float MinWaitTime
		{
			get => _spawnTimer.MinWaitTime;
			set => _spawnTimer.MinWaitTime = value;
		}

		[Export(PropertyHint.Range, "0,1,0.01,or_greater")] public float MaxWaitTime
		{
			get => _spawnTimer.MaxWaitTime;
			set => _spawnTimer.MaxWaitTime = value;
		}


		/// <summary>
		/// Raised when the aliens start to spawn. The argument is the number of aliens to spawn.
		/// </summary>
		public event EventHandler<int> OnSpawningStart;

		/// <summary>
		/// Raised when the aliens stop spawning. The argument is the number of aliens that have spawned.
		/// </summary>
		public event EventHandler<int> OnSpawningEnd;


		/// <summary>
		/// Preloads asyncronously all common type aliens.
		/// </summary>
		private IPreloadedResource<PackedScene>[] PreloadCommonAliens()
		{
			CommonAliens[] enumeratedAliens = (CommonAliens[])Enum.GetValues(typeof(CommonAliens));
			IPreloadedResource<PackedScene>[] commonAliens = new IPreloadedResource<PackedScene>[enumeratedAliens.Length];

			for(int i = 0; i < enumeratedAliens.Length; i++)
			{
				NodePath toLoad;
				switch(enumeratedAliens[i])
				{
					case CommonAliens.Fighter:
						toLoad = "res://src/aliens/Fighter/AlienFighter.tscn";
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}

				commonAliens[i] = Preloader.Preload<PackedScene>(toLoad, LoadMode.Preload);
			}

			return commonAliens;
		}

		private void SpawnAlien()
		{
			// If there are any spawn points.
			if(_spawnPoints.Length != 0)
			{
				// Extracts a random spawn point.
				SpawnPoint spawnPoint = _spawnPoints[GD.Randi() % _spawnPoints.Length];

				// Extracts a random alien.
				PackedScene alien = _commonAliens[GD.Randi() % _commonAliens.Length].Resource;

				spawnPoint.Spawn(alien, SpawnedScale);

				SpawnedAliens++;
				GD.Print("Spawned " + SpawnedAliens + "° alien.");

				if(SpawnedAliens >= AliensPerWave)
					SpawnStop();
			}
			else
				GD.Print("Couldn't spawn" + SpawnedAliens + 1 + "° alien, no spawn points!");
		}


		public SpawnPointController()
		{
			SpawnedAliens = 0;

			_spawnTimer = new RandomTimer();
		}

		public void SpawnStart()
		{
			_spawnTimer.Start();
			OnSpawningStart?.DereferredInvoke(this, AliensPerWave);
		}

		public void SpawnStop()
		{
			_spawnTimer.Stop(false);
			OnSpawningEnd?.DereferredInvoke(this, SpawnedAliens);
		}


		/// <summary>
		/// Called when the node enters the scene tree for the first time.
		/// </summary>
		public override void _Ready()
		{
			float df = GameManager.CurrentLevel.DifficultyFactor;

			AliensPerWave += Mathf.CeilToInt(AliensPerWave * df);

			// Inizializing the spawn timer.
			_spawnTimer.MinWaitTime -= _spawnTimer.MinWaitTime * df;
			if(_spawnTimer.MinWaitTime < 0.2F)
				_spawnTimer.MinWaitTime = 0.2F;
			_spawnTimer.MinWaitTime -= _spawnTimer.MaxWaitTime * df;
			if(_spawnTimer.MaxWaitTime < 0.7F)
				_spawnTimer.MaxWaitTime = 0.7F;

			_spawnTimer.OnTimeout += (s, e) => SpawnAlien();
			AddChild(_spawnTimer);

			/**
			 * Copying, since the returned godot array is updated every time a
			 * new children is added. And also, filtering by type since there are
			 * weird unlisted children which aren't spawn points.
			 */
			_spawnPoints = (GetChildren().OfType<SpawnPoint>()).ToArray();

			_commonAliens = PreloadCommonAliens();
		}
	}
}