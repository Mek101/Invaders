using Godot;

namespace Invaders
{
	public class Background : Sprite
	{
		/// <summary>
		/// Called when the node enters the scene tree for the first time.
		/// </summary>
		public Background()
		{
			GameManager.Background = this;
			ScrollServer2D.Subscribe(this);
		}

		protected override void Dispose(bool disposing)
		{
			ScrollServer2D.UnSubscribe(this);
			base.Dispose(disposing);
		}
	}
}
