using Godot;
using Godot.Extensions;
using System;
using Invaders.Assets;
using Invaders.Bullets;

namespace Invaders.Pit
{
	public class Gate : StaticBody2D, IHittable
	{
		/// <summary>
		/// Provides blinking capabilities.
		/// </summary>
		private BlinkProvider _blinker;

		/// <summary>
		/// Cached versions of the top side points.
		/// </summary>
		private Vector2 _topSideStartPoint;

		/// <summary>
		/// Cached versions of the top side points.
		/// </summary>
		private Vector2 _topSideEndPoint;


		/// <summary>
		/// The current health of the gate.
		/// </summary>
		[Export] public float Health;

		/// <summary>
		/// The sprite representing the gate itself.
		/// </summary>
		public Sprite Sprite { get; protected set; }


		public event EventHandler OnGateDestroyed;


		private void SetTopSidePoints()
		{
			Vector2 center = Sprite.GlobalPosition;

			float height = center.y - (Sprite.Texture.GetHeight() * Sprite.GlobalScale.y ) / 2;
			float halfWidth = (Sprite.Texture.GetWidth() * Sprite.GlobalScale.x) / 2;

			_topSideStartPoint = new Vector2(center.x - halfWidth, height);
			_topSideEndPoint = new Vector2(center.x + halfWidth, height);
		}


		/// <summary>
		/// Inflict damage to the gate.
		/// </summary>
		/// <param name="damage"></param>
		/// <returns></returns>
		public float TakeDamage(float damage)
		{
			Health -= damage;

			if(Health <= 0)
			{
				_blinker.Blink();
				OnGateDestroyed?.DereferredInvoke(this, EventArgs.Empty);
				QueueFree();
			}
			else
			{
				_blinker.BlinkFor(25F);
			}

			return damage;
		}

		public (Vector2 StartPoint, Vector2 EndPoint) GetTopSidePoints()
		{
			return (_topSideStartPoint, _topSideEndPoint);
		}

		public override void _Ready()
		{
			Sprite = GetNode<Sprite>("Sprite");

			ShaderMaterial copy = (ShaderMaterial)Sprite.Material.Duplicate();
			Sprite.Material = copy;
			_blinker = new ShaderMaterialWhiteBlink(copy);

			SetTopSidePoints();
		}
	}
}