using Godot;
using Godot.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace Invaders.Pit
{
	public class GateManager : Node
	{
		private ConcurrentQueue<Gate> _gates;

		private Gate _currentGate;

		/// <summary>
		/// Used to prevent multiple rises of OnAllGatesDetroyed.
		/// </summary>
		private bool _allGatesDestroyedCalled;


		public int Count => _gates.Count;

		public Gate CurrentGate => _currentGate;


		/// <summary>
		/// Raised when a gate is destroyed. Includes the the number of the gate destroyed.
		/// </summary>
		public event EventHandler<int> OnGateDestroyed;

		/// <summary>
		/// Raised when all gates have been destroyed.
		/// </summary>
		public event EventHandler OnAllGatesDestroyed;


		private bool TryAdvanceImpl()
		{
			bool advanced = _gates.TryDequeue(out _currentGate);

			if(advanced)
			{
				_currentGate.OnGateDestroyed += (s, e) => OnGateDestroyed(s, _gates.Count);
			}
			else if(!_allGatesDestroyedCalled)
			{
				_allGatesDestroyedCalled = true;
				OnAllGatesDestroyed?.DereferredInvoke(this, EventArgs.Empty);
			}

			return advanced;
		}

		/// <summary>
		/// Manages the gates in the current level.
		/// </summary>
		/// <param name="gates">A list of the existing gates, from the top to the bottom.</param>
		public GateManager(IEnumerable<Gate> gates)
		{
			_allGatesDestroyedCalled = false;
			_gates = new ConcurrentQueue<Gate>(gates);
		}

		public bool TryAdvance()
		{
			if(_currentGate != null && !_currentGate.IsQueuedForDeletion())
				_currentGate.QueueFree();

			return TryAdvanceImpl();
		}

		public override void _Ready()
		{
			TryAdvanceImpl();
		}
	}
}
