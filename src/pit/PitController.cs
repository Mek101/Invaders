using Godot;
using Godot.Extensions;
using System.Linq;
using System.Collections.Generic;

namespace Invaders.Pit
{
	public class PitController : Node2D
	{
		[Export] public short StartGatesCount = 1;

		[Export] public float PitScale = 1;

		/// <summary>
		/// Manages the gates in level local pit.
		/// </summary>
		public GateManager GateManager { get; protected set; }

		/// <summary>
		/// The height of each pit section.
		/// </summary>
		public float PitSectionHeight { get; protected set; }


		/// <summary>
		/// Builds the pit from the start point.
		/// </summary>
		private IEnumerable<PitSection> BuildPit()
		{
			PitSection[] pitSections = new PitSection[StartGatesCount];
			PackedScene pitSectionScene = Preloader.Preload<PackedScene>("res://src/pit/PitSection.tscn",
			 LoadMode.Now).Resource;
			Vector2 nextPitPosition = new Vector2();

			// Concatenates "StartGatesCount" pit sections.
			for(int i = 0; i < StartGatesCount; i++)
			{
				pitSections[i] = (PitSection)pitSectionScene.Instance();
				AddChild(pitSections[i]);
				pitSections[i].Position = nextPitPosition;
				pitSections[i].Scale = new Vector2(PitScale, PitScale);

				// For some reason the texture height is only half it's real height.
				nextPitPosition.y += pitSections[i].Texture.GetHeight() * PitScale;
			}

			PitSectionHeight = pitSections[0].Texture.GetHeight() * PitScale;

			return pitSections;
		}

		public override void _Ready()
		{
			// Building and concatenating the pit sections, making the pit itself.
			IEnumerable<PitSection> pitSections = BuildPit();

			// Extracting the gates from the pit and manage them.
			GateManager = new GateManager(pitSections.Select((s) => s.Gate));
			AddChild(GateManager);
			GateManager.OnGateDestroyed += (s, n) => GateManager.TryAdvance();
		}
	}
}