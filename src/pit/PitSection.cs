using Godot;

namespace Invaders.Pit
{
	public class PitSection : Sprite
	{
		public Gate Gate { get; protected set; }


		public override void _Ready()
		{
			Gate = GetNode<Gate>("Gate");
		}
	}
}
