using Godot;
using Godot.Extensions;
using System;
using Invaders.Bullets;

namespace Invaders.Turrets
{
	public abstract class Barrel : Sprite
	{
		private const string POSITION = "position";

		/// <summary>
		/// The tween chain managing the barrel's animation.
		/// </summary>
		private TweenChain _tweenChain;

		private AudioStreamPlayer _shootAudio;


		/// <summary>
		/// Barrel recoil percentage. 1.0 full retraction, 0 for none.
		/// </summary>
		[Export(PropertyHint.Range, "0.0,1.0")] public float Recoil;

		/// <summary>
		/// Barrel recoil animation duration in seconds.
		/// </summary>
		[Export] public float ReloadDelay;

		/// <summary>
		/// The bullet loaded scene.
		/// </summary>
		[Export] public PackedScene BulletScene;


		/// <summary>
		/// Raised as the shooting animation finishes.
		/// </summary>
		public event EventHandler OnShootFinished;


		/// <summary>
		/// Spawns a bullet. Inheriting classes should use this method inside
		/// their implementation of the Shoot method.
		/// </summary>
		/// <param name="point">Global coordinates where to spawn the bullet.</param>
		/// <param name="direction">Direction of the bullet.</param>
		protected void SpawnBullet(Vector2 point, Vector2 direction)
		{
			Bullet b = (Bullet)BulletScene.Instance();
			b.GlobalPosition = point;
			b.GlobalRotation = this.GlobalRotation;
			b.Direction = direction;
			BulletAttachServer.AddBullet(b);
			_shootAudio.Play();
		}


		/// <summary>
		/// Shooting implementation.
		/// </summary>
		/// <param name="direction"></param>
		public abstract void ShootImpl(Vector2 direction);

		/// <summary>
		/// Tells the turret to open fire. How and where spawn the bullet must
		/// be implemented by the inhereting class.
		/// </summary>
		/// <param name="direction">The normalized direction towards the bullet is heading.</param>
		public void ShootAt(Vector2 direction)
		{
			// Animates the barrel recoil.
			var _ = _tweenChain.StartAsync();
			ShootImpl(direction);
		}

		public override void _Ready()
		{
			// The barrel's lentgh it's half the texture width minus half the body's texture length.
			float length = (Texture.GetWidth() / 2) - 7;
			// Percentage of the barrel to recoil.
			float recoilSize = Recoil * length;
			// Establishes the number of pixels of which the barrel should retract.
			Vector2 recoilEnd = new Vector2(-recoilSize, Position.y);

			using(TweenChainBuilder builder = new TweenChainBuilder())
			{
				builder.AppendPropertyInterpolation(this, POSITION, Position, recoilEnd,
					ReloadDelay / 2, Tween.TransitionType.Back, Tween.EaseType.Out);
				builder.AppendPropertyInterpolation(this, POSITION, recoilEnd, Position,
					ReloadDelay / 2, Tween.TransitionType.Linear, Tween.EaseType.Out);

				_tweenChain = builder.BuildChain();
				_tweenChain.OnChainCompleated += (s, c) => OnShootFinished?.Invoke(this, EventArgs.Empty);
				AddChild(_tweenChain);
			}

			_shootAudio = GetNode<AudioStreamPlayer>("ShootAudio");
		}
	}
}
