using Godot;

namespace Invaders.Turrets
{
	/// <summary>
	/// Used to indicate over whitch side to shoot.
	/// </summary>
	public enum TurretSide : byte { Left, Right }

	/// <summary>
	/// Different type of turrets the dock can host.
	/// </summary>
	public enum TurretType { Mini, EMP, Flak, Rocket, ElettroMagnetic, Default = Mini }

	/// <summary>
	/// Turret base class, providing implementation for a generic turret.
	/// </summary>
	public abstract class BaseTurret : Sprite
	{
		/// <summary>
		/// Indicates if the barrel's recoil tweenchain animations have finished
		/// and the barrel can shoot again.
		/// </summary>
		private bool _canShoot;

		/// <summary>
		/// The turret's barrel.
		/// </summary>
		private Barrel _barrel;


		/// <summary>
		/// The enumerated type of the turret. Must be implemented by the inhereting class.
		/// </summary>
		public abstract TurretType TurretType { get; }

		/// <summary>
		/// Tells whenever the turret should be able to shoot. Use for game over. True by default.
		/// </summary>
		/// <value><c>true</c> if the turret will be able to shoot, <c>false</c> if it will stay idle.</value>
		public bool IsActive;

		/// <summary>
		/// The side in witch the turret is placed
		/// </summary>
		public TurretSide Side;


		/// <summary>
		/// Tries to shoot towards the given position and start the tweenchain
		/// animation if enabled.
		/// </summary>
		/// <param name="mousePos">The position towards to shoot.</param>
		public void TryShoot(Vector2 mousePos)
		{
			if(_canShoot && IsActive)
			{
				if(Input.IsActionPressed("fire_both")
				 || Side == TurretSide.Left && Input.IsActionPressed("fire_left")
				 || Side == TurretSide.Right && Input.IsActionPressed("fire_right"))
				{
					_canShoot = false;
					_barrel.ShootAt((mousePos - GlobalPosition).Normalized());
				}
			}
		}

		public override void _Ready()
		{
			SetProcessInput(true);
			_canShoot = true;

			_barrel = GetNode<Barrel>("Barrel");
			_barrel.OnShootFinished += (s, c) => _canShoot = true;

			IsActive = true;
			GameManager.CurrentLevel.OnGameOver += (s, e) => IsActive = false;
		}

		/// <summary>
		/// Called at every physics update.
		/// </summary>
		/// <param name="delta">Elapsed time since the previous update.</param>
		public override void _PhysicsProcess(float delta)
		{
			Vector2 mousePos = GetGlobalMousePosition();
			// Rotates the turret.
			LookAt(mousePos);

			/**
			 * Since the game is supposed to be played keeping the buttons pressed, instead of
			 * flooding the program with input events, firing is processed during the physics frame.
			 */
			TryShoot(mousePos);
		}
	}
}
