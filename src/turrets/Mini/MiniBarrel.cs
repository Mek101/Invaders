using Godot;
using System;

namespace Invaders.Turrets
{
	public sealed class MiniBarrel : Barrel
	{
		private ValueTuple<Node2D, Node2D> _barrelHeads;


		public override void ShootImpl(Vector2 direction)
		{
			SpawnBullet(_barrelHeads.Item1.GlobalPosition, direction);
			SpawnBullet(_barrelHeads.Item2.GlobalPosition, direction);
		}

		public override void _Ready()
		{
			base._Ready();
			_barrelHeads.Item1 = GetNode<Node2D>("Barrel1");
			_barrelHeads.Item2 = GetNode<Node2D>("Barrel2");
		}
	}
}
