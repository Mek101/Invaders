
namespace Invaders.Turrets
{
	public sealed class MiniTurret : BaseTurret
	{
		public override TurretType TurretType => TurretType.Mini;
	}

}