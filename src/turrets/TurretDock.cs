using Godot;

namespace Invaders.Turrets
{

	/// <summary>
	/// Dock cable of changing and hosting various types of turrets.
	/// </summary>
	public class TurretDock : Node2D
	{
		/// <summary>
		/// The side in witch the turret is placed.
		/// </summary>
		[Export] public TurretSide Side;

		/// <summary>
		/// Currently hosted turret.
		/// </summary>
		public BaseTurret Turret { get; protected set; }


		public void SetTurret(PackedScene scene, float scale)
		{
			// Removes the old turret node from the tree.
			if(Turret != null)
			{
				RemoveChild(Turret);
				Turret.QueueFree();
			}

			// Setting the new turret node.
			Turret = (BaseTurret)scene.Instance();
			Turret.Scale = new Vector2(scale, scale);
			Turret.Side = Side;
			AddChild(Turret);
		}
	}
}
