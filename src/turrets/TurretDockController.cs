using Godot;
using Godot.Extensions;
using System;
using System.Collections.Generic;

namespace Invaders.Turrets
{
	public class TurretDockController : Node2D
	{
		private Dictionary<TurretSide, TurretDock> _docks;


		[Export] public float TurretScale = 1;


		public TurretDockController()
		{
			_docks = new Dictionary<TurretSide, TurretDock>();
		}

		/// <summary>
		/// Sets the dock's turret.
		/// </summary>
		/// <param name="type">The type of turret to set.</param>
		/// <param name="side">The side in witch the turret resides.</param>
		public void LoadTurret(TurretType type, TurretSide side)
		{
			TurretDock dock = _docks[side];
			switch(type)
			{
				case TurretType.Mini:
					dock.SetTurret(Preloader.Preload<PackedScene>(
						"res://src/turrets/Mini/MiniTurret.tscn", LoadMode.Now).Resource, TurretScale);
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(type), "Invalid turret type: " + type.ToString());
			}
		}

		/// <summary>
		/// Called when the node enters the scene tree for the first time.
		/// </summary>
		public override void _Ready()
		{
			TurretDock left = GetNode<TurretDock>("LeftTurretDock");
			TurretDock right = GetNode<TurretDock>("RightTurretDock");

			_docks.Add(left.Side, left);
			_docks.Add(right.Side, right);

			LoadTurret(TurretType.Default, TurretSide.Left);
			LoadTurret(TurretType.Default, TurretSide.Right);
		}
	}
}
